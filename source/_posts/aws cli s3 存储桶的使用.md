---
title: aws cli s3 存储桶的使用
tags: [AWS, S3]
categories: AWS S3
date: 2022-04-19 11:03:00
top: 0
cover: https://p.sda1.dev/12/787ff6062121bf6efda3a3ca9dcf7ff0/s33.png
top_img: https://p.sda1.dev/12/b11c4ef52a92d2f4603a86785a560141/bg.jpg
---

### AWS CLI

* AWS 命令行界面 (CLI) 是用于管理 AWS 服务的统一工具。只通过一个工具进行下载和配置，您可以使用命令行控制多个 AWS 服务并利用脚本来自动执行这些服务。

### 准备工作

* 下载安装 AWSCLI
* 新建一个AWS S3 存储桶
* 配置一个用户授予该用户`AmazonS3FullAccess`权限

### 如何使用

* 设置初始化配置

```BASH
aws configure
```

* 文件列表

```BASH
aws s3 ls
```

* 上传文件
* --acl 设置文件访问权限

```BASH
aws s3 cp 1.txt s3://bucket/1.txt --acl public-read
```

* 获取文件访问地址

```BASH
aws s3 presign s3://bucket/1.txt
```

* 多账号切换访问

```BASH
aws s3 ls --profile account
```

#### 文档

* [aws s3 命令列表](https://docs.aws.amazon.com/cli/latest/reference/s3/index.html)
