---
title: MongoDB安装和使用
tags: [MongoDB]
categories: MongoDB
date: 2022-04-09 14:06:13
top: 0
cover: https://p.sda1.dev/12/ee21f42f3a8bf4cd4ce8d920732eb92f/mongndb.jpg
top_img: https://p.sda1.dev/12/b11c4ef52a92d2f4603a86785a560141/bg.jpg
---

#### MongoDB 安装

> 简介

* MongoDB是一个基于分布式文件存储的数据库。由C++语言编写。旨在为WEB应用提供可扩展的高性能数据存储解决方案。
* MongoDB是一个介于关系数据库和非关系数据库之间的产品，是非关系数据库当中功能最丰富，最像关系数据库的。它支持的数据结构非常松散，是类似json的bson格式，因此可以存储比较复杂的数据类型。Mongo最大的特点是它支持的查询语言非常强大，其语法有点类似于面向对象的查询语言，几乎可以实现类似关系数据库单表查询的绝大部分功能，而且还支持对数据建立索引。

> MYSQL 和 MongoDB 的差异

* MySQL是传统的关系型数据库，MongoDB则是非关系型数据库，也叫文档型数据库，是一种NoSQL的数据库。

> 优势：

* 1、在适量级的内存的MongoDB的性能是非常迅速的，它将热数据存储在物理内存中，使得热数据的读写变得十分快。
* 2、MongoDB的高可用和集群架构拥有十分高的扩展性。
* 3、在副本集中，当主库遇到问题，无法继续提供服务的时候，副本集将选举一个新的主库继续提供服务。
* 4、MongoDB的Bson和JSon格式的数据十分适合文档格式的存储与查询。

> 劣势：

* 1、4.0 版本之前不支持事务操作。
* 2、MongoDB占用空间过大。


> use 命令，引用库，库不存在时会创建一个库
> 创建一个超级管理员账户

```BASH
use admin
db.createUser({user:"root",pwd:"rootpassword",roles:[{ role: "root", db: "admin" }]})
```


> 创建一个自定义用户拥有指定库的所有权限

```BASH
db.createUser({
    user:"username",
    pwd:"password",
    roles:[{
        role:"dbOwner",
        db:"dbname"
    }]
});
```

> 删除一个用户

```BASH
db.system.users.remove({user: "username"})
```

> 打开MongoDB授权验证

```BASH
bind_ip = 0.0.0.0
port = 27017
auth = true
```

> linux 下连接MongoDB 命令

```BASH
mongo 127.0.0.1:27017 -u username -p --authenticationDatabase auth_db
```
