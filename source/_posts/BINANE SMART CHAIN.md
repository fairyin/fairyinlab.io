---
title: BINANE SMART CHAIN
tags: [BINANE]
categories: BINANE SMART CHAIN
date: 2022-05-06 15:42:00
top: 0
cover: https://p.sda1.dev/12/6595ac618b7d3761fa918debd958fbac/ba.jpg
top_img: https://p.sda1.dev/12/b11c4ef52a92d2f4603a86785a560141/bg.jpg
---

#### 准备工作

* `1`台`16`核`64G`内存的`linux`服务器,`3.5T`以上硬盘
* [MBR 分区为 GPT](https://aws.amazon.com/cn/premiumsupport/knowledge-center/ec2-ubuntu-convert-mbr-to-gpt/)
* 下载历史快照
* Database after Ancient Data Prune
* 修剪过历史数据的，不能查询历史交易
* Pruned database
* 保留历史交易查询的数据块
* 这里我们下载 Pruned database 可查询历史交易的快照
* [https://github.com/bnb-chain/bsc-snapshots](https://github.com/bnb-chain/bsc-snapshots)
* [https://docs.binance.org/smart-chain/developer/fullnode.html#steps-to-run-a-fullnode](https://docs.binance.org/smart-chain/developer/fullnode.html#steps-to-run-a-fullnode)
* 安装多线程下载软件

```BASH
sudo apt-get install aria2
```

#### 下载最近的历史快照

```BASH
screen -S download

aria2c -o geth.tar.lz4 -x 16 -s 20 "https://tf-dex-prod-public-snapshot-site1.s3-accelerate.amazonaws.com/geth-20220503.tar.lz4?AWSAccessKeyId=AKIAYINE6SBQPUZDDRRO&Signature=%2B83JSYmxAiGVnr4qAQGvpFOvDgI%3D&Expires=1654256806"
```

#### 解压文件

```BASH
screen -S jieya

lz4 -d geth.tar.lz4 | tar -xv

crtl + a + d

# 检查目前所有的screen作业，并删除已经无法使用的screen作业
screen -wipe

# 恢复离线的screen作业

screen -r job
```

#### 下载`geth_linux`可执行文件

```BASH
wget   $(curl -s https://api.github.com/repos/bnb-chain/bsc/releases/latest |grep browser_ |grep geth_linux |cut -d\" -f4)
mv geth_linux /usr/local/bin/geth
```

#### 下载`mainnet.zip`配置文件

```BASH
wget   $(curl -s https://api.github.com/repos/bnb-chain/bsc/releases/latest |grep browser_ |grep mainnet |cut -d\" -f4)
unzip mainnet.zip
```

* 解压之后会有`config.toml`和`genesis.json`文件
* 修改`config.toml`文件，允许访问

```BASH
HTTPHost = "0.0.0.0"
HTTPVirtualHosts = ["*"]
```

#### 启动

* 使用`supervisor`守护进程启动

```BASH
[program:geth_job]
command=geth --config /bsc/config.toml  --datadir /bsc/node --diffsync --rpcapi eth,web3,admin,personal,net --cache 18000 --rpc.allow-unprotected-txs --txlookuplimit 0
user=root
autostart=true
autorestart=true
numprocs=1
redirect_stderr=true
stdout_logfile=/bsc/job/geth.log
```

#### 测试`rpc`服务
```BASH
curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_syncing","params":[],"id":1}' http://127.0.0.1:8080
```

#### 以太坊JSON RPC手册

* [http://cw.hubwiz.com/card/c/ethereum-json-rpc-api/1/3/19/](http://cw.hubwiz.com/card/c/ethereum-json-rpc-api/1/3/19/)


#### 掉块处理

* 停止钱包服务，更新对等节点
* [https://api.binance.org/v1/discovery/peers](https://api.binance.org/v1/discovery/peers)

```BASH
StaticNodes = [
    "enode://9f90d69c5fef1ca0b1417a1423038aa493a7f12d8e3d27e10a5a8fd3da216e485cf6c15f48ee310a14729bc3a4b05038479476c0aa82eed3c5d9d2e64ba3a2b3@52.69.42.169:30311",
    "enode://78ef719ebb2f4fc222aa988a356274dcd3624fb808936ca2ea77388ca229773d4351f795abf505e86db1a30ed1523ded9f9674d916b295bfb98516b78d2844be@13.231.200.147:30311"
]
```
* 重启服务
